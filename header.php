<script src="https://code.createjs.com/1.0.0/createjs.min.js"></script>
<!-- <script src="<?php echo get_template_directory_uri(); ?>/image/logoAnimation.js"></script> -->

<style>
	.navbar-brand .cls-1 {
	}

	.navbar-brand .cls-1 {
	}
</style>
<script>
	$(function(){
		// $('.cls-1').addClass('animate__animated animate__rubberBand animate__infinite animate__slower');
		// console.log($('.cls-1'));
	});
</script>
<nav class="navbar navbar-expand-lg navbar-light">
	<a class="navbar-brand" href="/">
		<!-- <div id="animation_container" style="background-color:rgba(255, 255, 255, 1.00); width:500px; height:500px">
			<canvas id="canvas" width="100" height="100" style="position: absolute; display: block; background-color:rgba(255, 255, 255, 1.00);"></canvas>
			<div id="dom_overlay_container" style="pointer-events:none; overflow:hidden; width:500px; height:500px; position: absolute; left: 0px; top: 0px; display: block;">
			</div>
		</div> -->
		<img id="qfLogo" src="<?php echo get_template_directory_uri(); ?>/image/tungWahLogo.png" />
	</a>
	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#mainNavBar" aria-controls="mainNavBar" aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>
	<div class="collapse navbar-collapse" id="mainNavBar">
		<ul class="navbar-nav ml-auto">
			<?php
			$menu_name = 'main-nav';
			$menuLocations = get_nav_menu_locations();
			$menu = wp_get_nav_menu_object($menuLocations['primary']);
			$menuitems = wp_get_nav_menu_items($menu->term_id);

			//dump($menuitems);
			foreach ($menuitems as $item) {
				$id = get_post_meta($item->ID, '_menu_item_object_id', true);
				$page = get_page($id);
				// $link = get_page_link($id);
			?>
				<? if ($page->post_title=="Login"){ ?>
				<? if (is_user_logged_in()){ ?>
				<li class="nav-item">
					<a class="nav-link" href="<?= wp_logout_url("https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]") ?>" style="<? if (get_the_title()==$page->post_title){ ?>color: #EC1A2E;<? } ?>">Logout</a>
				</li>
				<? } else { ?>
				<li class="nav-item">
					<a class="nav-link" href="<?= wp_login_url("https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]") ?>" style="<? if (get_the_title()==$page->post_title){ ?>color: #EC1A2E;<? } ?>">Login</a>
				</li>
				<? } ?>
				<? }else{ ?>
				<li class="nav-item">
					<a class="nav-link" href="<?php echo $item->url; ?>" style="<? if (get_the_title()==$page->post_title){ ?>color: #EC1A2E; border-bottom: 2px solid #EC1A2E;<? } ?>"><?php echo $item->title; ?></a>
				</li>
				<? } ?>
			<?php
			}
			?>
		</ul>
		<!-- <form class="form-inline my-2 my-lg-0">
			<input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
			<button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
		</form> -->
	</div>
</nav>
