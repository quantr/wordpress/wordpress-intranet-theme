<?php

function qf_theme_init()
{
    $locations = array(
        'primary'  => __('Primary', 'qf')
    );
    register_nav_menus($locations);
}

add_action('init', 'qf_theme_init');

function qf_theme_after_setup()
{
    //echo 'qf_theme_setup';
}

add_action('after_setup_theme', 'qf_theme_after_setup');

function dump($obj)
{
    echo "<pre style='color: white;'>";
    var_dump($obj);
    echo "</pre>";
}

function qf_widgets_init() {
    register_sidebar( array(
      'name'          => __( 'QF Footer Widgets', 'qf-child' ),
      'id'            => 'qf-sidebar-1',
      'description'   => __( 'Add widgets here to appear in your footer area.', 'qf-child' ),
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget'  => '</aside>',
      'before_title'  => '<h2 class="widget-title">',
      'after_title'   => '</h2>',
    ) );
  }
  add_action( 'widgets_init', 'qf_widgets_init' );
