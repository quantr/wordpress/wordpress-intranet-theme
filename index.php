<?php
error_reporting(E_ALL);
$pageId = get_the_ID();
?>
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Quantr Foundation</title>
  <?php wp_head(); ?>

  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/library/bootstrap-4.5.3-dist/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/style.css">
  <script src="<?php echo get_template_directory_uri(); ?>/library/jquery-3.5.1.min.js"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/library/bootstrap-4.5.3-dist/js/bootstrap.min.js"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/library/svg-inject.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.2/css/all.min.css" integrity="sha512-1sCRPdkRXhBV2PBLUdRb4tMg1w2YPf37qatUFeS7zlBy7jJI8Lf4VHwWfZZfpXtYSLy85pkm9GaYVYMfw5BC1A==" crossorigin="anonymous" referrerpolicy="no-referrer" />

  <script type="text/javascript">
    (function(c, l, a, r, i, t, y) {
      c[a] = c[a] || function() {
        (c[a].q = c[a].q || []).push(arguments)
      };
      t = l.createElement(r);
      t.async = 1;
      t.src = "https://www.clarity.ms/tag/" + i;
      y = l.getElementsByTagName(r)[0];
      y.parentNode.insertBefore(t, y);
    })(window, document, "clarity", "script", "4by6h79fut");
  </script>
</head>
<?
$page = get_page($pageId);
?>
<body>
  <div id="mainContainer" class="container">
    <div class="container-fluid d-flex flex-column <? if ($page->post_type != 'post') { ?>lionRockDiv<?}?>">
      <div class="row">
        <div class="col">
          <?php
          get_header();
          ?>
        </div>
      </div>
      <div class="row">
        <div class="col">
          <span class="departmentButton">IT</span>
          <span class="departmentButton">HR</span>
          <span class="departmentButton">Sales</span>
          <span class="departmentButton">Finance</span>
        </div>
      </div>
      <div class="row <? if ($page->post_type != 'post') { ?>mainBodyRow<? } ?>">
        <div class="col">
          <? if ($page->post_type == 'post') { ?>
            <div class="mainPost">
              <button id="backButton" class="btn btn-primary" onclick="location.href='/blog';"></button>
              <span class="postTitle"><?= $page->post_title ?></span>
              <span class="postDate"><?= $page->post_date ?></span>
            </div>
          <? } ?>
          <?php
          // echo '<pre>';
          // var_dump($page);
          // echo '</pre>';
          $content = apply_filters('the_content', $page->post_content);
          echo $content;
          ?>
        </div>
      </div>
      <div class="row footerRow">
        <div class="col p-0">
          <?php
          get_footer();
          ?>
          <?php if (is_active_sidebar('qf-sidebar-1')) : ?>
            <div class="widget-area" role="complementary">
              <?php dynamic_sidebar('qf-sidebar-1'); ?>
            </div>
          <?php endif; ?>
        </div>
      </div>
    </div>
  </div>
</body>

</html>