<div class="footer container-fluid">
	<div class="row">
		<div class="col">
			<?php
			wp_footer();
			?>
		</div>
	</div>
	<div class="row">
		<div class="col">
			Tung Wah, Copyright @<?= Date('Y') ?>
		</div>
		<div class="col-8" style="text-align: right;">
			<span class="footerMenu">News Center</span>
			<span class="footerMenu">Department</span>
			<span class="footerMenu"><a hred="https://wordpress-intranet.quantr.hk/index.php/org-chart/">Org Chart</a></span>
			<a href="https://gitlab.com/quantr/toolchain" target="_blank"><img src="<?= get_template_directory_uri() ?>/image/gitlab.png" class="footerMsIcon" /></a>
			<img src="<?= get_template_directory_uri() ?>/image/ms icon/powerautomate.png" class="footerMsIcon" />
			<img src="<?= get_template_directory_uri() ?>/image/ms icon/powerbi.png" class="footerMsIcon" />
			<img src="<?= get_template_directory_uri() ?>/image/ms icon/teams.png" class="footerMsIcon" />
			<img src="<?= get_template_directory_uri() ?>/image/ms icon/powerapps.png" class="footerMsIcon" />
			<a href="https://wordpress-intranet.quantr.hk/index.php/survey/"><img src="<?= get_template_directory_uri() ?>/image/ms icon/microsoft forms.png" class="footerMsIcon" /></a>
		</div>
	</div>
</div>